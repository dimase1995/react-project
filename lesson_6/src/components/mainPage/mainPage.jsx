import { Header } from "../header/header";
import logo from "../../img/rick-and-morty.png";
import "./mainPage.css";
//import { useState } from "react";
//import { useEffect } from "react";
export function MainPage({ name, linkName }) {
  /*
  const [name, setName] = useState([]);
  const [linkName, setLinkName] = useState([]);
  // const [test, setTest] = useState([]);
  useEffect(() => {
    fetch("https://rickandmortyapi.com/api")
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Помилка запиту");
      })
      .then((data) => {
        //let arr = Object.entries(data);

        let arrName = Object.values(data);
        let arrLinkName = Object.keys(data);

        setName(arrLinkName);
        setLinkName(arrName);
        // console.log(arrName);
        // console.log(arrLinkName);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
*/
  return (
    <>
      <Header name={name} linkName={linkName} />
      <div className="imgLogo">
        <img src={logo} />
      </div>
      <div className="search">
        <input
          type="search"
          placeholder="🔎  Filter by name or episode (ex. S01 or S01E02)"
        ></input>
      </div>
    </>
  );
}
//
