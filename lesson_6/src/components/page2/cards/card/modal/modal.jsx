import { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { Else } from "./else/else";
import "./modal.css";
export function Modal({
  title,
  close,
  data,
  img,
  name,
  gender,
  species,
  alive,
  episodes,
  location,
  residents,
  characters,
}) {
  const [resident, setResident] = useState([]);
  const [character, setCharacters] = useState([]);
  const { pathname } = useLocation();
  // console.log(residents);
  useEffect(() => {
    if (title === "location") {
      residents.map((el) => {
        fetch(`${el}`)
          .then((response) => response.json())
          .then((data) => {
            setResident((prev) => [...prev, data]);
          })
          .catch((error) => {
            console.error(error);
          });
      });
    }
  }, [pathname]);
  useEffect(() => {
    if (title === "episode") {
      characters.map((el) => {
        fetch(`${el}`)
          .then((response) => response.json())
          .then((data) => {
            setCharacters((prev) => [...prev, data]);
          })
          .catch((error) => {
            console.error(error);
          });
      });
    }
  }, []);

  // console.log(resident);

  if (title === "character") {
    return (
      <div
        className="window"
        onClick={() => {
          close(false);
        }}
      >
        <div
          className="modal"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <img src={img} />
          <div>Name: {name}</div>
          <div>Gender: {gender}</div>
          <div>Species: {species}</div>
          <div>Status: {alive}</div>
          <Else episode={episodes} location={location} />
        </div>
      </div>
    );
  } else if (title === "location") {
    //console.log(title);

    return (
      <div
        className="window test"
        onClick={() => {
          close(false);
        }}
      >
        <div
          className="modal modLoc"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          {resident.map((el) => (
            <div className="location__card">
              <div className="loc__img">
                <img src={el.image} />
                <div className="heroName">{el.name}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  } else if (title === "episode") {
    return (
      <div
        className="window test"
        onClick={() => {
          close(false);
        }}
      >
        <div
          className="modal modLoc"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          {character.map((el) => (
            <div className="location__card">
              <div className="loc__img">
                <img src={el.image} />
                <div className="heroName">{el.name}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
