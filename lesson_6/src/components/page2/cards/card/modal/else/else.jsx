import { useEffect } from "react";
import { useState } from "react";
import { ModalLocEpis } from "../../modalLocationEpisodes/modalLocEpis";
import "./else.css";

export function Else({ episode, location }) {
  const [showEpis, setShowEpis] = useState(false);
  const [showLoc, setShowLoc] = useState(false);
  const [episInfo, setEpisInfo] = useState([]);
  const [locInfo, setLocInfo] = useState({});
  const [name, setName] = useState([]);
  // console.log(episode);
  // console.log(location);
  useEffect(() => {
    fetch(location.url)
      .then((response) => response.json())
      .then((data) => {
        setLocInfo(data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  useEffect(() => {
    episode.map((el) => {
      fetch(`${el}`)
        .then((response) => response.json())
        .then((data) => {
          setEpisInfo((prev) => [...prev, data]);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }, []);
  return (
    <>
      <div className="info">
        <button
          onClick={() => {
            setShowLoc(true);
            setName("LOCATIONS");
          }}
        >
          LOCATIONS
        </button>
        <button
          onClick={() => {
            setShowEpis(true);
            setName("EPISODES");
          }}
        >
          EPISODES
        </button>
      </div>
      {showLoc && <ModalLocEpis title={name} locInfo={locInfo} />}
      {showEpis && <ModalLocEpis title={name} episInfo={episInfo} />}
    </>
  );
}
