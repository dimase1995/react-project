import "./modalLocEpis.css";
export function ModalLocEpis({ episInfo, locInfo, title }) {
  //console.log(episInfo);
  if (title === "EPISODES") {
    return (
      <div className="modalWind">
        {episInfo.map((el, i) => (
          <div key={el.id} className="card__Episode">
            <div>Name: {el.name}</div>
            <div>Air_date: {el.air_date}</div>
            <div>Episode: {el.episode}</div>
          </div>
        ))}
      </div>
    );
  } else if (title === "LOCATIONS") {
    return (
      <div className="modalWind">
        <div key={locInfo.id} className="card__Episode">
          <div>Name: {locInfo.name}</div>
          <div>Dimension: {locInfo.dimension}</div>
          <div>Type: {locInfo.type}</div>
        </div>
      </div>
    );
  }
}
