import { useState } from "react";
import { Modal } from "./modal/modal";
import "./card.css";

export function Card({
  title,
  name,
  img,
  data,
  gender,
  species,
  alive,
  type,
  dimension,
  episode,
  location,
  air_date,
  residents,
  characters,
}) {
  const [modal, setModal] = useState(false);

  if (title === "character") {
    return (
      <>
        <div
          className="card hover"
          onClick={() => {
            setModal(true);
          }}
        >
          <p>{name}</p>
          <img src={img}></img>
        </div>
        {modal && (
          <Modal
            title={title}
            name={name}
            img={img}
            data={data}
            gender={gender}
            species={species}
            alive={alive}
            episodes={episode}
            location={location}
            close={setModal}
          />
        )}
      </>
    );
  } else if (title === "location") {
    // console.log(data);
    return (
      <>
        <div
          className="card hover"
          onClick={() => {
            setModal(true);
          }}
        >
          <p>{name}</p>
          <div>Type: {type}</div>
          <div>Dimension: {dimension}</div>
        </div>
        {modal && (
          <Modal
            data={data}
            close={setModal}
            title={title}
            residents={residents}
          />
        )}
      </>
    );
  } else if (title === "episode") {
    return (
      <>
        <div
          className="card hover"
          onClick={() => {
            setModal(true);
          }}
        >
          <p>{name}</p>
          <div>Air date: {air_date}</div>
          <div>Episode: {episode}</div>
        </div>
        {modal && (
          <Modal
            data={data}
            close={setModal}
            title={title}
            characters={characters}
          />
        )}
      </>
    );
  }
}
