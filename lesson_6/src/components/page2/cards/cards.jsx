import { useState } from "react";
import { Card } from "./card/card";
import "./cards.css";
export function Cards({ data, title }) {
  return (
    <div className="cards">
      {data &&
        data.map((el, i) => (
          <Card
            title={title}
            key={el.id}
            name={el.name}
            img={el.image}
            gender={el.gender}
            species={el.species}
            alive={el.status}
            data={data}
            type={el.type}
            dimension={el.dimension}
            episode={el.episode}
            location={el.location}
            air_date={el.air_date}
            residents={el.residents}
            characters={el.characters}
          />
        ))}
    </div>
  );
}
