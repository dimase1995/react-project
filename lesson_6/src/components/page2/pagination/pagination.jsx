import { useEffect, useState } from "react";
import "./pagination.css";
export function Pagination({ plus, minus, page }) {
  return (
    <>
      <div className="footer">
        <button onClick={minus}>PREV</button>
        <div className="numPage">Page №: {page}</div>
        <button onClick={plus}>NEXT</button>
      </div>
    </>
  );
}
