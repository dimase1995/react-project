import { useState } from "react";
import { useEffect } from "react";
import { Header } from "../header/header";
import { Cards } from "./cards/cards";
import { Pagination } from "./pagination/pagination";
import { useLocation } from "react-router-dom";
export function PageTwo({ title, name }) {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [fullPage, setFullPage] = useState(null);
  let { pathname } = useLocation();
  const plus = () => {
    setPage((prev) => ++prev);
  };

  const minus = () => {
    setPage((prev) => --prev);
  };
  useEffect(() => {
    if (page <= 0) {
      setPage(1);
    }
    if (page > fullPage) {
      setPage(fullPage);
    }
    fetch(`https://rickandmortyapi.com/api/${title}/?page=${page}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data.results);
        setFullPage(data.info.pages);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [page]);

  useEffect(() => {
    setPage(1);
    fetch(`https://rickandmortyapi.com/api/${title}/?page=${page}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data.results);
        setFullPage(data.info.pages);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [pathname]);
  return (
    <div>
      <Header name={name} />
      <Cards data={data} title={title} />
      <Pagination page={page} plus={plus} minus={minus} />
    </div>
  );
}
