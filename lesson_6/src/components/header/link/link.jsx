import { Link } from "react-router-dom";

export function Links({ title, url }) {
  return (
    <div className="linkNav">
      <Link to={`/${title}`.slice(0, -1)}>
        <div>{title}</div>
      </Link>
    </div>
  );
}
