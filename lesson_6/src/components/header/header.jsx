import Logo from "../../img/logoHeader.png";
import { Link } from "react-router-dom";
import "./header.css";
import { Navigation } from "./nav/nav";
export function Header({ name, linkName }) {
  return (
    <header className="header">
      <div>
        <Link to={`/`}>
          <img src={Logo} alt="logo" />
        </Link>
      </div>
      <div className="links">
        <Navigation title={name} url={linkName} />
      </div>
    </header>
  );
}
