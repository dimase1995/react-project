import { Links } from "../link/link";
import "./nav.css";
export function Navigation({ title, url }) {
  return (
    <ul className="navigation">
      {title.map((el, i) => (
        <li key={i}>
          <Links url={el} title={el}></Links>
        </li>
      ))}
    </ul>
  );
}
