import { MainPage } from "../mainPage/mainPage";
import { BrowserRouter } from "react-router-dom";
import { Route, Routes } from "react-router";
import { PageTwo } from "../page2/page2";
import { useState } from "react";
import { useEffect } from "react";
export function App() {
  const [name, setName] = useState([]);
  const [linkName, setLinkName] = useState([]);
  // const [test, setTest] = useState([]);
  useEffect(() => {
    fetch("https://rickandmortyapi.com/api")
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Помилка запиту");
      })
      .then((data) => {
        //let arr = Object.entries(data);

        let arrName = Object.values(data);
        let arrLinkName = Object.keys(data);

        setName(arrLinkName);
        setLinkName(arrName);
        // console.log(arrName);
        // console.log(arrLinkName);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <div>
      {
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<MainPage name={name} linkName={linkName} />}
            ></Route>
            <Route
              path="/character"
              element={<PageTwo name={name} title="character" />}
            ></Route>
            <Route
              path="/location"
              element={<PageTwo name={name} title="location" />}
            ></Route>
            <Route
              path="/episode"
              element={<PageTwo name={name} title="episode" />}
            ></Route>
          </Routes>
        </BrowserRouter>
      }
    </div>
  );
}
