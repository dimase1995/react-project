import React from "react";
import ReactDOM from "react-dom";
import Card from "./components";

function App() {
  return <Card />;
}
ReactDOM.render(<App />, document.querySelector(".root"));
