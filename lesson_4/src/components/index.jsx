import { Component } from "react";
import { render } from "react-dom";
import "../components/styles.css";
class Card extends Component {
  state = {
    items: [],
  };

  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ items: data });
      });
  }

  render() {
    console.log(this.state.items);
    return (
      <div className="content">
        {this.state.items.map((el) => (
          <div className="cards" key={el.id}>
            <div className="category">category: {el.category}</div>
            <div className="img">
              <img src={el.image} alt={el.category} />
            </div>
            <div className="title">{el.title}</div>
            <div className="test">
              <div className="description">{el.description}</div>
            </div>
            <div className="count">quantity of goods:{el.rating.count}</div>
            <div className="price">price:{el.price}$</div>
          </div>
        ))}
      </div>
    );
  }
}
export default Card;
