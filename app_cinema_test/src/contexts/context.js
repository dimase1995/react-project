import { createContext, useCallback, useEffect, useState } from "react";
import { getFilmsConfig, getPersonsConfig } from "../config/api";

export const CinemaContext = createContext();

export const CinemaContextProvider = ({ children }) => {
  const [state, setState] = useState({
    persons: [],
    films: [],
    time: [],
    flag: false,
  });

  const [selectedPerson, setSelectedPerson] = useState(() => {
    const savedData = localStorage.getItem("myDataP");
    return savedData ? JSON.parse(savedData) : null;
  });
  const [selectedFilm, setSelectedFilm] = useState(() => {
    const savedData = localStorage.getItem("myDataF");
    return savedData ? JSON.parse(savedData) : null;
  });

  const getPersons = useCallback(async () => {
    await fetch("https://api.themoviedb.org/3/person/popular", getPersonsConfig)
      .then((response) => response.json())
      .then((response) => {
        setState((prev) => ({ ...prev, persons: response.results }));
      })
      .catch((err) => console.error(err));
  }, []);

  const getFilms = useCallback(async () => {
    await fetch("https://api.themoviedb.org/3/discover/movie", getFilmsConfig)
      .then((response) => response.json())
      .then((response) => {
        setState((prev) => ({ ...prev, films: response.results }));
      })
      .catch((err) => console.error(err));
  }, []);
  const selectPerson = (person) => {
    localStorage.setItem("myDataP", JSON.stringify(person));
    setSelectedPerson(person);
  };
  const selectFilm = (film) => {
    localStorage.setItem("myDataF", JSON.stringify(film));
    setSelectedFilm(film);
  };

  useEffect(() => {
    getPersons();
    getFilms();
  }, []);

  return (
    <CinemaContext.Provider
      value={{
        state,
        setState,
        getFilms,
        getPersons,
        selectedFilm,
        selectFilm,
        selectPerson,
        selectedPerson,
      }}
    >
      {children}
    </CinemaContext.Provider>
  );
};
