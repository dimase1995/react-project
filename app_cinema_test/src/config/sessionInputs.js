export const sessionInputs = [
  {
    id: "book1",
    inputType: "radio",
    name: "contact",
    value: "10:00",
  },
  {
    id: "book2",
    inputType: "radio",
    name: "contact",
    value: "12:30",
  },
  {
    id: "book3",
    inputType: "radio",
    name: "contact",
    value: "16:00",
  },
  {
    id: "book4",
    inputType: "radio",
    name: "contact",
    value: "18:00",
  },
];
