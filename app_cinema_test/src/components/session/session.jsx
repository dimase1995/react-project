import { useCinema } from "../../hooks/useCinema";
import { sessionInputs } from "../../config/sessionInputs";
import "./session.css";

export function Session() {
  const { state, setState } = useCinema();

  return (
    <>
      <div className="session">
        <form>
          {sessionInputs.map((input) => (
            <div key={input.id}>
              <input
                type={input.inputType}
                id={input.id}
                name={input.name}
                value={input.value}
                onChange={(event) =>
                  setState((prev) => ({
                    ...prev,
                    time: [event.target.value],
                    flag: true,
                  }))
                }
              />
              <label htmlFor={input.id}>{input.value}</label>
            </div>
          ))}
        </form>
      </div>
    </>
  );
}
