import React from "react";
import "./dashboard.css";
import logo from "../../img/logoboard.svg";
function Dashboard() {
  return (
    <div className="dashboard">
      <img src={logo} alt="" />
      <a href="#">Dashboard</a>
    </div>
  );
}
export default Dashboard;
