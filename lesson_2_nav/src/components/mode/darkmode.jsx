import React from "react";
import "./darkmode.css";
import logo from "../../img/darkmode.svg";
import icon from "../../img/icon.png";
function Darkmode() {
  return (
    <div className="darkmode">
      <img src={logo} alt="" />
      <a href="#">Darkmode</a>
      <div className="switch">
        <span>
          <img src={icon}></img>
        </span>
      </div>
    </div>
  );
}
export { Darkmode };
