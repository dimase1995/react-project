import React from "react";
import "./search.css";
function Search() {
  return (
    <div className="search">
      <input type="search" placeholder="🔎 Search..."></input>
    </div>
  );
}
export { Search };
