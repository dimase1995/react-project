import React from "react";
import "./notification.css";
import logo from "../../img/notification.svg";

function Notification() {
  return (
    <div className="notification">
      <img src={logo} alt="" />
      <a href="#">Notification</a>
    </div>
  );
}
export { Notification };
