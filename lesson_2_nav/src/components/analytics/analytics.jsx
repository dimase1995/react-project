import React from "react";
import "./analytics.css";
import logo from "../../img/analytic.svg";

function Analytics() {
  return (
    <div className="analytic">
      <img src={logo} alt="" />
      <a href="#">Analytics</a>
    </div>
  );
}
export { Analytics };
