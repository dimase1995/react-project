import React from "react";
import "./inventory.css";
import logo from "../../img/inventory.svg";

function Inventory() {
  return (
    <div className="inventory">
      <img src={logo} alt="" />
      <a href="#">Inventory</a>
    </div>
  );
}
export { Inventory };
