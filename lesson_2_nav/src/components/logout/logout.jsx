import React from "react";
import "./logout.css";
import logo from "../../img/logout.svg";

function Logout() {
  return (
    <div className="logout">
      <img src={logo} alt="" />
      <a href="#">Logout</a>
    </div>
  );
}
export { Logout };
