import React from "react";
import "./header.css";
function Header() {
  return (
    <div className="header">
      <div className="logo">AF</div>
      <div className="name">
        <div>AnimatedFred</div>
        <a href="mailto:animated@demo.com" target="">
          animated@demo.com
        </a>
      </div>
    </div>
  );
}
export { Header };
