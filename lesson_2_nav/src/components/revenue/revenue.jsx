import React from "react";
import "./revenue.css";
import logo from "../../img/revenue.svg";

function Revenue() {
  return (
    <div className="revenue">
      <img src={logo} alt="" />
      <a href="#">Revenue</a>
    </div>
  );
}
export { Revenue };
