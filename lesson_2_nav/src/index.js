import React from "react";
import ReactDOM from "react-dom";
import Dashboard from "./components/dashboard/dashboard";
import { Header } from "./components/header_nav/header";
import { Revenue } from "./components/revenue/revenue";
import { Search } from "./components/search/search";
import { Notification } from "./components/notifications/notification";
import { Analytics } from "./components/analytics/analytics";
import { Inventory } from "./components/inventory/inventory";
import { Logout } from "./components/logout/logout";
import { Darkmode } from "./components/mode/darkmode";

function Navigation() {
  return (
    <>
      <div className="nav_page">
        <Header></Header>
        <Search></Search>
        <div className="components">
          <Dashboard></Dashboard>
          <Revenue></Revenue>
          <Notification></Notification>
          <Analytics></Analytics>
          <Inventory></Inventory>
        </div>
        <div className="footer">
          <Logout></Logout>
          <Darkmode></Darkmode>
        </div>
      </div>
    </>
  );
}

ReactDOM.render(<Navigation />, document.querySelector(".page"));
