import { Planet } from "../planet/planet";
import "../starSky/starSky.css";
function StarSky({ click }) {
  console.log(click);
  return (
    <>
      <div className="stars"></div>
      <div className="twinkling"></div>
      <div className="earth">
        <Planet click={click} />
      </div>
    </>
  );
}

export { StarSky };
