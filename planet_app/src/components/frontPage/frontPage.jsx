import vector from "../../img/Vector.png";
import { Planet } from "../planet/planet";
import { Page2 } from "../page2/page2";
import { useState } from "react";
import { useEffect } from "react";
import "../frontPage/frontPage.css";
function FrontPage() {
  const [showPage, fn] = useState(false);

  useEffect(() => {
    let parallaxObject = document.querySelector(".parallax-object");
    let parallaxSpeed = 0.1;

    const handleScroll = () => {
      const scrollPosition = window.pageYOffset;
      parallaxObject.style.transform = `translateY(${
        scrollPosition * parallaxSpeed
      }px)`;
      console.log();
    };

    window.addEventListener("scroll", handleScroll);
  }, []);

  return (
    <>
      <div className="main__page">
        <div className="stars"></div>
        <div className="twinkling"></div>

        <header className="header">
          <ul>
            <li>Inicio</li>
            <li>Espacio</li>
            <li>Planetas</li>
            <li>Nosotros</li>
            <li>Misiones</li>
          </ul>
        </header>
        <div className="fon ">
          <div className="fon__text">Un viaje al espacio</div>
          <Planet></Planet>
        </div>
        <div
          className="vector"
          onClick={() => {
            fn(true);
          }}
        >
          <img src={vector} alt="vector" />
        </div>
      </div>
      {showPage && <Page2 />}
    </>
  );
}
export default FrontPage;
