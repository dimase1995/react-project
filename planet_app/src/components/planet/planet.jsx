import planet from "../../img/mask.png";
import "../planet/planet.css";
export function Planet({ click, style }) {
  console.log(style);
  return (
    <div className={click ? "img__planet" : "planet parallax-object"}>
      <img
        className="pl"
        src={planet}
        alt="planet"
        onClick={(e) => {
          console.log(e);
          if (click) {
            click(true);
          }
        }}
      />
    </div>
  );
}
