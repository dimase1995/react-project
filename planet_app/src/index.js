import React, { useState } from "react";
import ReactDOM from "react-dom";
import FrontPage from "./components/frontPage/frontPage";
import { StarSky } from "./components/starSky/starSky";

function App() {
  const [modal, setModal] = useState(false);

  return <>{modal ? <FrontPage /> : <StarSky click={setModal} />}</>;
}

ReactDOM.render(<App />, document.querySelector(".root"));
