import React from "react";
import ReactDOM from "react-dom";

function Table() {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Зодіак:</th>
          <th>Овен</th>
          <th>Телець</th>
          <th>Близнюки</th>
          <th>Рак</th>
          <th>Лев</th>
          <th>Діва</th>
          <th>Терези</th>
          <th>Скорпіон</th>
          <th>Стрілець</th>
          <th>Козоріг</th>
          <th>Водолій</th>
          <th>Риби</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Тривалість:</td>
          <td>21 березня - 20 квітня</td>
          <td>21 квітня - 21 травня</td>
          <td>22 травня - 21 червня</td>
          <td>22 червня - 22 липня</td>
          <td>23 липня - 22 серпня</td>
          <td>23 серпня - 23 вересня</td>
          <td>24 вересня - 23 жовтня</td>
          <td>24 жовтня - 22 листопада</td>
          <td>23 листопада - 21 грудня</td>
          <td>22 грудня - 20 січня</td>
          <td>21 січня - 18 лютого</td>
          <td>19 лютого - 20 березня</td>
        </tr>
      </tbody>
    </table>
  );
}
ReactDOM.render(<Table></Table>, document.querySelector(".one"));
