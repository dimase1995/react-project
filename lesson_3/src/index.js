import React, { useState } from "react";
import ReactDOM from "react-dom";
import { BtnAdd } from "../src/components/btn_add/btn_add";
function App() {
  return (
    <>
      <BtnAdd />
    </>
  );
}
ReactDOM.render(<App />, document.querySelector(".app"));
