import React, { useState } from "react";
import "../btn_add/style.css";
import { ModaleWindow } from "../module_window/module_window";
function BtnAdd() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <button
        type="button"
        className={!isOpen ? "btn_add" : "none"}
        onClick={() => setIsOpen(true)}
      >
        add element
      </button>
      {isOpen && <ModaleWindow close={setIsOpen} />}
    </>
  );
}
export { BtnAdd };
