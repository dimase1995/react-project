import React from "react";
import "./form.css";
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      products: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    //this.toggleDone = this.toggleDone.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleDelete(id) {
    this.setState((prevState) => ({
      products: prevState.products.filter((product) => product.id !== id),
    }));
  }

  handleEdit(title) {
    this.setState({ value: title });
  }

  toggleDone(id) {
    this.setState((elem) => ({
      products: elem.products.map((el) => {
        if (el.id === id) {
          el.toggle = true;
        }
        return {
          ...el,
          toggle: el.toggle,
        };
      }),
    }));
  }

  handleSubmit(event) {
    this.setState({ value: "" });
    this.state.products.push({
      id: Math.random().toString(36).substring(7),
      title: this.state.value,
      toggle: false,
    });
    console.log(this.state.products);
    event.preventDefault();
  }

  render() {
    return (
      <>
        <form className="form" onSubmit={this.handleSubmit}>
          <input
            type="text"
            maxLength="30"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <input type="submit" value="ADD" />
        </form>
        <ul className="ul">
          {this.state.products.map((product) => (
            <li className={product.toggle ? "done" : ""} key={product.id}>
              <span>{product.title}</span>
              <button
                onClick={() => {
                  this.toggleDone(product.id);
                }}
              >
                DONE
              </button>
              <button
                onClick={() => {
                  this.handleEdit(product.title);

                  this.handleDelete(product.id);
                }}
              >
                EDIT
              </button>
              <button
                onClick={() => {
                  this.handleDelete(product.id);
                }}
              >
                DELETE
              </button>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export { Form };
