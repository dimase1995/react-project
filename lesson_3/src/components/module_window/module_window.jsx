import "../module_window/style.css";
import { Form } from "../form/form";

function ModaleWindow({ close }) {
  return (
    <div className="window" onClick={() => close(false)}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <Form />
      </div>
    </div>
  );
}
export { ModaleWindow };
