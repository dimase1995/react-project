import React from "react";
import { connect } from "react-redux";
import { startTimer, stopTimer } from "../timerReducer/timerReducer.js";

const TimerControls = ({ isRunning, startTimer, stopTimer }) => {
  const handleStart = () => {
    if (!isRunning) {
      startTimer();
    }
  };

  const handleStop = () => {
    if (isRunning) {
      stopTimer();
    }
  };

  return (
    <div>
      <button onClick={handleStart} disabled={isRunning}>
        Start
      </button>
      <button onClick={handleStop} disabled={!isRunning}>
        Stop
      </button>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isRunning: state.isRunning,
});

const mapDispatchToProps = {
  startTimer,
  stopTimer,
};

export default connect(mapStateToProps, mapDispatchToProps)(TimerControls);
