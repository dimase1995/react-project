const ActionTypes = {
  SET_TIMER: "SET_TIMER",
  START_TIMER: "START_TIMER",
  STOP_TIMER: "STOP_TIMER",
  DECREMENT_TIME: "DECREMENT_TIME",
};

export const setTimer = (time) => ({
  type: ActionTypes.SET_TIMER,
  payload: time,
});

export const startTimer = () => ({
  type: ActionTypes.START_TIMER,
});

export const stopTimer = () => ({
  type: ActionTypes.STOP_TIMER,
});
export const decrementTime = () => ({
  type: ActionTypes.DECREMENT_TIME,
});

//

const initialState = {
  time: 0,
  isRunning: false,
};
export const timerReducer = (state = initialState, action) => {
  if (action.type === ActionTypes.SET_TIMER) {
    return {
      ...state,
      time: action.payload,
    };
  }

  if (action.type === ActionTypes.START_TIMER) {
    return {
      ...state,
      isRunning: true,
    };
  }

  if (action.type === ActionTypes.STOP_TIMER) {
    return {
      ...state,
      isRunning: false,
    };
  }

  if (action.type === ActionTypes.DECREMENT_TIME) {
    return {
      ...state,
      time: state.time > 0 ? state.time - 1 : 0,
    };
  }

  return state;
};
