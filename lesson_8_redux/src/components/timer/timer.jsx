import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  setTimer,
  startTimer,
  stopTimer,
  decrementTime,
} from "../timerReducer/timerReducer.js";
import TimerControls from "../timerControls/timerControls";
import TimerDisplay from "../timerDisplay/timerDisplay";
import alarmSound from "../../Trill.mp3";
import "./timer.css";
const Timer = ({
  time,
  isRunning,
  setTimer,
  startTimer,
  stopTimer,
  decrementTime,
}) => {
  const [inputValue, setInputValue] = useState("");
  const [audio] = useState(new Audio(alarmSound));
  useEffect(() => {
    let interval;

    if (isRunning) {
      interval = setInterval(() => {
        decrementTime();
      }, 1000);
      if (time === 0) {
        clearInterval(interval);
        stopTimer();
        audio.play();
      }
    }

    return () => {
      clearInterval(interval);
    };
  }, [isRunning, time, decrementTime]);

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleStart = () => {
    setTimer(parseInt(inputValue, 10));
    startTimer();
  };

  return (
    <div className="main">
      <div className="timer">
        <div className="set_timer">
          <div>Set Timer (seconds):</div>
          <input type="number" value={inputValue} onChange={handleChange} />
          <button onClick={handleStart} disabled={isRunning}>
            Set Timer
          </button>
        </div>
        <TimerDisplay />
        <TimerControls />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  time: state.time,
  isRunning: state.isRunning,
});

const mapDispatchToProps = {
  setTimer,
  startTimer,
  stopTimer,
  decrementTime,
};

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
