import React from "react";
import { connect } from "react-redux";

const TimerDisplay = ({ time }) => {
  const formatTime = (seconds) => {
    if (seconds !== undefined && seconds !== null) {
      console.log(seconds);
      return seconds.toString();
    } else {
      return "";
    }
  };

  return (
    <div>
      <div>Time Remaining: {formatTime(time)}</div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  time: state.time,
});

export default connect(mapStateToProps)(TimerDisplay);
