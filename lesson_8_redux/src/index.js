import { legacy_createStore } from "redux";
import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { timerReducer } from "./components/timerReducer/timerReducer.js";
import Timer from "./components/timer/timer.jsx";

const initialState = JSON.parse(localStorage.getItem("timerState")) || {};
const store = legacy_createStore(timerReducer, initialState);

store.subscribe(() => {
  const state = store.getState();
  localStorage.setItem("timerState", JSON.stringify(state));
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <Timer />
  </Provider>
);
