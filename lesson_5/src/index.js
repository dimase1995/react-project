import React from "react";
import ReactDOM from "react-dom";
import Headers from "./components/headers/headers";
import FonPage from "./components/fon/fonPage";
import Project from "./components/projects/project";
import Footer from "./components/footer/footer";
import "../src/index.css";
function Portfolio() {
  return (
    <div>
      <Headers />
      <FonPage />
      <Project />
      <Footer />
    </div>
  );
}

ReactDOM.render(<Portfolio />, document.querySelector(".root"));
