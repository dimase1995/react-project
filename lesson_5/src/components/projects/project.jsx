import "../projects/project.css";
import barber from "./img/barber.png";
import pizza from "./img/pizza.png";
import shop from "./img/shop.png";
function Project() {
  return (
    <div className="project__main__page">
      <div id="project" className="project">
        Project
      </div>
      <div className="project__1">
        <div className="proj_txt_1">
          <h2>Barbershop</h2>
          <p>
            Development of a barbershop web page with the help of HTML, CSS.
          </p>
          <button
            onClick={() => {
              window.open(
                "https://gitlab.com/dimase1995/barbershop-project",
                "_blank"
              );
            }}
          >
            View Project
          </button>
        </div>
        <div className="imgProj">
          <img src={barber} alt="" />
        </div>
      </div>
      <div className="project__2">
        <div className="proj_txt_2">
          <h2>Pizza</h2>
          <p>
            Development of the web page of the online pizza shop with the help
            of HTML, CSS, JavaScript. This project used the Drag'n'Drop event.
          </p>
          <button
            onClick={() => {
              window.open(
                "https://gitlab.com/dimase1995/pizza-project",
                "_blank"
              );
            }}
          >
            View Project
          </button>
        </div>
        <div className="imgProj">
          <img src={pizza} alt="" />
        </div>
      </div>
      <div className="project__3">
        <div className="proj_txt_3">
          <h2>Сlothing store</h2>
          <p>
            Development of an online clothing store using HTML, CSS, JavaScript.
            This project has functionalities like product filter, get product
            from database and work basket.
          </p>
          <button
            onClick={() => {
              window.open(
                "https://gitlab.com/dimase1995/shorts-store-project",
                "_blank"
              );
            }}
          >
            View Project
          </button>
        </div>
        <div className="imgProj">
          <img src={shop} alt="" />
        </div>
      </div>
    </div>
  );
}
export default Project;
