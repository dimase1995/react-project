import "../headers/headers.css";
function Headers() {
  return (
    <header className="header">
      <div className="header__left">
        <a href="./">Dmitry Denysenko</a>
      </div>
      <div className="header__right">
        <a href="#about">About</a>
        <a href="#project">Projects</a>
        <a href="#contact">Contacts</a>
      </div>
    </header>
  );
}
export default Headers;
