import "../footer/footer.css";
import mail from "./img/mail.png";
import insta from "./img/insta.png";
import lin from "./img/lin.png";
function Footer() {
  return (
    <footer id="contact" className="footer">
      <div className="logos">
        <a href="https://www.instagram.com/dmitrydenysenko/" target="_blank">
          <img src={insta} alt="" />
        </a>
        <a
          href="https://www.linkedin.com/in/dmitry-denysenko-7b11b827a/"
          target="_blank"
        >
          <img src={lin} alt="" />
        </a>
        <a href="mailto:dimase1995@gmail.com" target="_blank">
          <img src={mail} alt="" />
        </a>
      </div>
      <a className="link" href="./">
        Dmitry Denysenko 2023
      </a>
    </footer>
  );
}
export default Footer;
