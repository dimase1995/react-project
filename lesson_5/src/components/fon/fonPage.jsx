import "../fon/fonPage.css";
import avatar from "./img/avatar.png";
function FonPage() {
  return (
    <div className="fon">
      <div className="fon__txt" id="about">
        <div className="front__dev">Front-End Developer</div>
        <h1>Hello, my name is Dmitry Denysenko </h1>
        <p>
          I am a beginner web application developer. During the training course,
          I studied the following areas: JavaScript, React, HTML, CSS, Fetch,
          Bootstrap, Photoshop, Figma, Git/GitLab, Trello.
        </p>
        <button className="btn__proj">Projects</button>
        <button
          className="btn__linkIn"
          onClick={() => {
            window.open(
              "https://www.linkedin.com/in/dmitry-denysenko-7b11b827a/",
              "_blank"
            );
          }}
        >
          LinkedIn
        </button>
      </div>
      <div className="img">
        {" "}
        <img src={avatar} alt="" />{" "}
      </div>
    </div>
  );
}
export default FonPage;
